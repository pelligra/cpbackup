#############################

 # cPanel Backup Optimized
 # by leon © 2017
 # cpbackup 1.3.1 - 21 Sept 2017

# 0 3 */2 * * /root/cpbackup.sh > /dev/null 2>&1
# 
############################
#
# Changelog
# lock file
# rsync --update
# ssh port
# check ssh connection
# local backup
################# CONFIG ########################
# config.cpbackup
################################################

source /root/cpbackup/config.cpbackup
today=$(date "+%Y.%m.%d_%H.%M.%S")
rslog="/var/log/cpbackup/cpbackup.$today.log"
rsync_bin="/usr/local/cpanel/bin/cpuwatch 10 /usr/bin/rsync"
mkdir -p /home/temp/
mkdir -p /var/log/cpbackup/
ssh_port=22
SECONDS=0
lock="/root/cpbackup/lock"
start=$(date +%d-%m-%Y.%H.%M.%S)

#check lock
if [ -f "$lock" ]
	then 
    	echo "Lock file exist!" |mail -s "[Backup Failure]: $HOSTNAME" $email
		exit
fi

#check ssh connection
if [ $IP != "local" ]
then
	ssh -q -o BatchMode=yes -o ConnectTimeout=10 $IP exit
	if [ $? -ne 0 ]
		then
		echo "Can not connect to $IP" | mail -s "[Backup failed]: $HOSTNAME" $email 
		exit
	fi
fi
touch $lock

for user in `ls -1 /var/cpanel/users|grep -v system`; 
do 
	HOMEDIR=$(egrep "^${user}:" /etc/passwd |awk -F ":" {'print $6'})
	echo "Homedir: $HOMEDIR"
	if [ ! -f /var/cpanel/users/$user ]; then
    echo "$user user file missing, likely an invalid user" >> $rslog
  elif [ "$HOMEDIR" == "" ];then
    echo "Couldn't determine home directory for $user" >> $rslog
  else
	/scripts/pkgacct  --incremental --backup --skiphomedir $user $TEMP >> $rslog

	if [ $IP = "local" ]
	then
		${rsync_bin} -avp --update --log-file="$rslog" $TEMP/$user $DEST/
		${rsync_bin} -avp --update --delete --log-file="$rslog" --exclude={'wp-content/cache/*','cache/smarty/cache/*',tmp/*,temp/*,error_log,softaculous_backups/*,back*.tar.gz,.trash/*,.cagefs/*,'cache/smarty/compile/*','cachefs/*'} $HOMEDIR/ $DEST/$user/homedir
	else
		${rsync_bin} -avp --update --bwlimit=$rsync_limit --log-file="$rslog" -e "ssh -o StrictHostKeyChecking=no -p $ssh_port" $TEMP/$user root@$IP:$DEST/
		${rsync_bin} -avp --update --bwlimit=$rsync_limit --delete --log-file="$rslog" --exclude={'wp-content/cache/*','cache/smarty/cache/*',tmp/*,temp/*,error_log,softaculous_backups/*,back*.tar.gz,.trash/*,.cagefs/*,'cache/smarty/compile/*','cachefs/*'} -e "ssh -o StrictHostKeyChecking=no -p $ssh_port" $HOMEDIR/ root@$IP:$DEST/$user/homedir
	fi

	rm -rf $TEMP/$user
	fi
done

# Backup system configuration file
tar cvfz $TEMP/etc.tar.gz /etc >> $rslog

if [ $IP = "local" ]
    then
		${rsync_bin} -avp --log-file="$rslog" $TEMP/etc.tar.gz $DEST/../system/
	else
		${rsync_bin} -avp --bwlimit=$rsync_limit --log-file="$rslog" -e "ssh -o StrictHostKeyChecking=no -p $ssh_port" $TEMP/etc.tar.gz root@$IP:$DEST/../system/
fi


db_error=$(grep "Failed to dump database" $rslog)
duration=$(date -u -d @${SECONDS} +"%T")
end=$(date "+%Y/%m/%d %H:%M:%S")

if [ ! -z "$db_error" ];  
    then     
        echo -e "Backup finished with errors on $HOSTNAME.\n\nErrors:\n${db_error}\n\nStart: $start\nEnd: $end\n\nDuration: $duration elapsed.\n\nLogs: $rslog" |mail -s "[WARNING: Backup finished with errors]: $HOSTNAME" $email
    else
        echo -e "Backup finished on $HOSTNAME.\n\nStart: $start\nEnd: $end\n\nDuration: $duration elapsed.\n\nLogs: $rslog" |mail -s "[Backup finished]: $HOSTNAME" $email
fi

echo -e "=====================================================\n$end - Finished" >> $rslog
rm -rf $lock
